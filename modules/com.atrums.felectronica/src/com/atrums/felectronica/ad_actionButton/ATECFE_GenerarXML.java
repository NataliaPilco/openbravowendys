package com.atrums.felectronica.ad_actionButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;

import com.atrums.felectronica.process.ATECFE_Funciones_Aux;

public class ATECFE_GenerarXML extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  ATECFE_Funciones_Aux opeaux = new ATECFE_Funciones_Aux();

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String strInvoice = vars.getStringParameter("inpcInvoiceId");
    String strRetencion = vars.getStringParameter("inpcoRetencionCompraId");
	String strGuia = vars.getStringParameter("inpmInoutId");

    if (!strInvoice.equals("") && strInvoice != null && !strInvoice.equals("null")) {
      imprXML(response, strInvoice);
    } else if (!strRetencion.equals("") && strRetencion != null && !strRetencion.equals("null")) {
      imprXMLRet(response, strRetencion);
    } else if (!strGuia.equals("") && strGuia != null && !strGuia.equals("null")) {
      imprXMLGuia(response, strGuia);
    } else {
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println("");
      out.close();
    }
  }

  protected void imprXML(HttpServletResponse response, String strInvoice) throws IOException,
      ServletException {

    byte[] btyDoc = null;
    ATECFEGenerarPDFData[] ateData = ATECFEGenerarPDFData.methodSeleccionarInvo(this, strInvoice);

    if (ateData.length > 0) {
      btyDoc = ateData[0].dato1.getBytes();
    }

    File flDocXML = null;

    if (btyDoc != null) {

      flDocXML = opeaux.bytetofile(btyDoc);
      if (flDocXML != null) {
        FileInputStream insStream = new FileInputStream(flDocXML);

        response.setContentType("application/xml");
        response.setHeader("Content-Disposition", "attachment; filename=Documento_"
            + ateData[0].dato2 + ".xml");
        IOUtils.copy(insStream, response.getOutputStream());
        flDocXML.exists();
        insStream.close();
        response.flushBuffer();
      }
    }
  }

  protected void imprXMLRet(HttpServletResponse response, String strRetencio) throws IOException,
      ServletException {

    byte[] btyDoc = null;
    ATECFEGenerarPDFData[] ateData = ATECFEGenerarPDFData.methodSeleccionarRete(this, strRetencio);

    if (ateData.length > 0) {
      btyDoc = ateData[0].dato1.getBytes();
    }

    File flDocXML = null;

    if (btyDoc != null) {

      flDocXML = opeaux.bytetofile(btyDoc);
      if (flDocXML != null) {
        FileInputStream insStream = new FileInputStream(flDocXML);

        response.setContentType("application/xml");
        response.setHeader("Content-Disposition", "attachment; filename=Documento_"
            + ateData[0].dato2 + ".xml");
        IOUtils.copy(insStream, response.getOutputStream());
        flDocXML.exists();
        insStream.close();
        response.flushBuffer();
      }
    }
  }
  
  protected void imprXMLGuia(HttpServletResponse response, String strGuia) throws IOException,
      ServletException {

    byte[] btyDoc = null;
    ATECFEGenerarPDFData[] ateData = ATECFEGenerarPDFData.methodSeleccionarGuia(this, strGuia);

    if (ateData.length > 0) {
      btyDoc = ateData[0].dato1.getBytes();
    }

    File flDocXML = null;

    if (btyDoc != null) {

      flDocXML = opeaux.bytetofile(btyDoc);
      if (flDocXML != null) {
        FileInputStream insStream = new FileInputStream(flDocXML);

        response.setContentType("application/xml");
        response.setHeader("Content-Disposition", "attachment; filename=Documento_"
            + ateData[0].dato2 + ".xml");
        IOUtils.copy(insStream, response.getOutputStream());
        flDocXML.exists();
        insStream.close();
        response.flushBuffer();
      }
    }
  }
}
