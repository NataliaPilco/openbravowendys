/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2013 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package org.openbravo.model.common.invoice;

import java.math.BigDecimal;

import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Virtual entity class to hold computed columns for entity Invoice.
 *
 * NOTE: This class should not be instantiated directly.
 */
public class Invoice_ComputedColumns extends BaseOBObject implements ClientEnabled, OrganizationEnabled {
    private static final long serialVersionUID = 1L;
    public static final String ENTITY_NAME = "Invoice_ComputedColumns";
    
    public static final String PROPERTY_ATECFECATORCE = "atecfeCatorce";
    public static final String PROPERTY_ATECFECERO = "atecfeCero";
    public static final String PROPERTY_ATECFEDOCE = "atecfeDoce";
    public static final String PROPERTY_ATECFEIVACATORCE = "atecfeIvaCatorce";
    public static final String PROPERTY_ATECFEIVACERO = "atecfeIvaCero";
    public static final String PROPERTY_ATECFEIVADOCE = "atecfeIvaDoce";
    public static final String PROPERTY_ATECFETOTALIVA = "atecfeTotalIva";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public BigDecimal getAtecfeCatorce() {
      return (BigDecimal) get(PROPERTY_ATECFECATORCE);
    }

    public void setAtecfeCatorce(BigDecimal atecfeCatorce) {
      set(PROPERTY_ATECFECATORCE, atecfeCatorce);
    }
    public BigDecimal getAtecfeCero() {
      return (BigDecimal) get(PROPERTY_ATECFECERO);
    }

    public void setAtecfeCero(BigDecimal atecfeCero) {
      set(PROPERTY_ATECFECERO, atecfeCero);
    }
    public BigDecimal getAtecfeDoce() {
      return (BigDecimal) get(PROPERTY_ATECFEDOCE);
    }

    public void setAtecfeDoce(BigDecimal atecfeDoce) {
      set(PROPERTY_ATECFEDOCE, atecfeDoce);
    }
    public BigDecimal getAtecfeIvaCatorce() {
      return (BigDecimal) get(PROPERTY_ATECFEIVACATORCE);
    }

    public void setAtecfeIvaCatorce(BigDecimal atecfeIvaCatorce) {
      set(PROPERTY_ATECFEIVACATORCE, atecfeIvaCatorce);
    }
    public BigDecimal getAtecfeIvaCero() {
      return (BigDecimal) get(PROPERTY_ATECFEIVACERO);
    }

    public void setAtecfeIvaCero(BigDecimal atecfeIvaCero) {
      set(PROPERTY_ATECFEIVACERO, atecfeIvaCero);
    }
    public BigDecimal getAtecfeIvaDoce() {
      return (BigDecimal) get(PROPERTY_ATECFEIVADOCE);
    }

    public void setAtecfeIvaDoce(BigDecimal atecfeIvaDoce) {
      set(PROPERTY_ATECFEIVADOCE, atecfeIvaDoce);
    }
    public BigDecimal getAtecfeTotalIva() {
      return (BigDecimal) get(PROPERTY_ATECFETOTALIVA);
    }

    public void setAtecfeTotalIva(BigDecimal atecfeTotalIva) {
      set(PROPERTY_ATECFETOTALIVA, atecfeTotalIva);
    }
    public Client getClient() {
      return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
      set(PROPERTY_CLIENT, client);
    }
    public Organization getOrganization() {
      return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
      set(PROPERTY_ORGANIZATION, organization);
    }
}
